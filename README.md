# OneFi NodeJS Environment Variable Decryptor


This library is originally designed for use with AWS Lambda functions. To use with other node applications, make sure to do

```npm install aws-sdk --save```

The job is simple: loop through all environment variables, look for values that match `^kms:\s*`, strip out the prefix and decrypt.

This basically ensures we do not expose sensitive values like db passwords, api keys, etc.

