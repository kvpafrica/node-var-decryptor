/**
 * Created by nonami on 12/02/2018.
 */
const AWS = require('aws-sdk');
const KMS = new AWS.KMS();
const SSM = new AWS.SSM();

const ENC_VAR_PREFIX = 'kms:';
const ENC_VAR_REGEX = new RegExp('^' + ENC_VAR_PREFIX + '\s*');

const decryptAndSwap = (varName, haltOnFail) => {
  if (!process.env[varName]) {
    return;
  }
  const params = {
    CiphertextBlob: new Buffer(process.env[varName].replace(ENC_VAR_REGEX, ''), 'base64')
  };

  return new Promise(function (resolve, reject) {
    KMS.decrypt(params, function (err, data) {
      if (err) {
        console.error(err);
        if (haltOnFail) {
          reject(err);
        } else {
          resolve(null);
        }
      } else {
        process.env[varName] = data.Plaintext.toString();
        resolve(process.env[varName]);
      }
    });
  });
};

const fetchDecryptAndSwap = (varNames, haltOnFail) => {
  const Names = [];
  const variablesKeyMap = {};

  let withDecryption = false;

  varNames.forEach((varName) => {
    let ssmKey = process.env[varName].replace(/^ssm:\s*/, '');
    if (/~true$/.test(ssmKey)) {
      withDecryption = true;
      ssmKey = ssmKey.replace(/~true$/, '');
    }
    variablesKeyMap[ssmKey] = varName;
    Names.push(ssmKey);
  });


  const params = {
    Names: Names,
    WithDecryption: withDecryption
  };

  return new Promise(function (resolve, reject) {
    SSM.getParameters(params, function (err, data) {
      if (err) {
        console.error(err);
        if (haltOnFail) {
          reject(err);
        } else {
          return;
        }
      }

      data.Parameters.forEach(parameter => {
        const varName = variablesKeyMap[parameter.Name];
        process.env[varName] = parameter.Value ;
      });

      resolve(data);
    });
  });
};


class VarDecryptor {

  constructor() {

  }

  isEncryptedVariable(varName) {
    return ENC_VAR_REGEX.test(process.env[varName]);
  }

  isSsmKey(varName) {
    const regex = new RegExp('^ssm:*');
    return regex.test(process.env[varName]);
  };

  /**
   * Decrypts all encrypted variables
   * @param haltOnFail Boolean
   * @returns {Promise.<*>} Returns Native promise, please DO NOT try using bluebird's SPREAD
   */
  decryptEnvironmentVariables(haltOnFail = false) {
    const promises = [];
    for (let i in process.env) {
      if (this.isEncryptedVariable(i)) {
        promises.push(decryptAndSwap(i, haltOnFail));
      }
    }
    return Promise.all(promises);
  }

  fetchSsmParameters(haltOnFail = false) {
    const promises = [];

    let keys = [];
    for (let i in process.env) {
      if (this.isSsmKey(i)) {
        keys.push(i);
      }
    }

    const batches = chunk(keys, 10);
    batches.forEach((batch) => {
      promises.push(fetchDecryptAndSwap(batch, haltOnFail));
    });
    return Promise.all(promises);
  }
}


const chunk = (arr, size) => {
  const chunks = [];
  for(let i=0, j=arr.length; i < j; i+= size) {
    chunks.push(arr.slice(i, i + size));
  }
  return chunks;
};


module.exports = VarDecryptor;